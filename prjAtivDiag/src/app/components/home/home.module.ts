import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { TopoComponent } from './topo/topo.component';
import { IndicacoesComponent } from './indicacoes/indicacoes.component';
import { CarrosselComponent } from './carrossel/carrossel.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    HomeComponent,
    TopoComponent,
    IndicacoesComponent,
    CarrosselComponent,
    FooterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomeModule { }
